import asyncio
import tomllib

from botops import Bot, Dispatcher, MessageHandler


class EchoHandler(MessageHandler):
    async def handle(self) -> None:
        await self.answer(self.update.text)


async def main():
    dispatcher = Dispatcher()
    dispatcher.register(EchoHandler)

    with open("config.toml", "rb") as file:
        config = tomllib.load(file)

    async with Bot(config["bot"]["token"], dispatcher) as bot:
        await bot.run()


if __name__ == "__main__":
    asyncio.run(main())
